import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppColor {
  static const Color primaryColor = Color(0xFF403A7A); // buat warna primary
  static const Color secondaryColor = Color(0xFF897CFF); // warna secondary
  static const Color backgroundColor = Color(0xFF6A62B7); // warna background
}

class AppTextStyle {
  // ini buat style title nya pake google fonts marriweather
  static final TextStyle titleTextStyle = GoogleFonts.merriweather().copyWith(
    fontSize: 32,
    fontWeight: FontWeight.w700,
  );

  // ini buat style typograpy nya pake google fonts sourceSans3
  static final TextStyle typograpyTextStyle =
      GoogleFonts.sourceSans3().copyWith(
    fontSize: 18,
    fontWeight: FontWeight.w400,
  );
}

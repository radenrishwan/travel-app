import 'package:flutter/material.dart';
import 'package:travel_app/const.dart';

class TravelNoteView extends StatelessWidget {
  const TravelNoteView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(80),
        child: AppBar(
          toolbarHeight: 80,
          automaticallyImplyLeading: false,
          flexibleSpace: Container(),
          leading: Container(),
          backgroundColor: AppColor.primaryColor,
          title: Text(
            'Travel Notes',
            style: AppTextStyle.titleTextStyle
                .copyWith(fontSize: 28, color: Colors.white),
          ),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(38),
              bottomRight: Radius.circular(38),
            ),
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 36, vertical: 45),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Mount Fuji',
              style: AppTextStyle.titleTextStyle.copyWith(
                fontSize: 20,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.7,
              child: Stack(
                // pake stack buat text sama garis
                children: [
                  // generate 30 garis
                  for (int i = 0; i < 30; i++)
                    // set posisi setiap garis supaya pas, pake left sama right 0 supaya nempel ke kanan kiri
                    // top nya ditambah i setiap loop supaya pas ke bawahnya
                    Positioned(
                      left: 0,
                      right: 0,
                      top: 6 + (i + 1) * 32,
                      child: Container(
                        width: double.infinity,
                        height: 0.8,
                        color: Colors.black,
                      ),
                    ),
                  const TextField(
                    decoration: InputDecoration(border: InputBorder.none),
                    cursorHeight: 24,
                    cursorColor: AppColor.primaryColor,
                    style: TextStyle(
                      fontSize: 16,
                      height: 2,
                    ),
                    keyboardType: TextInputType
                        .multiline, // set keyboard type ke multiline, jadi enternya buat new line bukan done
                    expands: true, // expandnya true supaya textfieldnya lebar
                    maxLines:
                        null, // maxlines set null supaya textfieldnya bisa banyak garis
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:travel_app/const.dart';
import 'package:travel_app/view/travel_notes_view.dart';

class TravelPlanView extends StatelessWidget {
  const TravelPlanView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(80),
        child: AppBar(
          toolbarHeight: 80,
          automaticallyImplyLeading: false,
          flexibleSpace: Container(),
          leading: Container(),
          title: Text(
            'Travel Plan',
            style: AppTextStyle.titleTextStyle.copyWith(fontSize: 28),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          padding: const EdgeInsets.symmetric(
            horizontal: 24,
            vertical: 48,
          ),
          decoration: const BoxDecoration(
            color: AppColor.primaryColor,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(38),
              topRight: Radius.circular(38),
            ),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            // generate 5 card buat list travel plan
            children: List.generate(5, (index) {
              return InkWell(
                onTap: () {
                  // pake navigator buat pindah ke travel note kalau dipencet
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) {
                      return const TravelNoteView();
                    }),
                  );
                },
                child: Container(
                  padding: const EdgeInsets.only(
                    top: 28,
                    bottom: 28,
                    left: 38,
                    right: 26,
                  ),
                  margin: const EdgeInsets.only(bottom: 38),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(37),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        'Mount Fuji',
                        style: AppTextStyle.titleTextStyle.copyWith(
                          fontWeight: FontWeight.w700,
                          fontSize: 18,
                        ),
                      ),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Transform.flip(
                            child: const Icon(
                              Icons.access_time,
                              size: 24,
                              color: Colors.black,
                            ),
                          ),
                          const SizedBox(width: 7),
                          Text(
                            '5 Days',
                            style: AppTextStyle.typograpyTextStyle.copyWith(
                              fontWeight: FontWeight.w600,
                              fontSize: 16,
                            ),
                          ),
                          const SizedBox(width: 20),
                          InkWell(
                            borderRadius: BorderRadius.circular(13.5),
                            onTap: () {},
                            child: Container(
                              height: 37,
                              width: 33,
                              decoration: BoxDecoration(
                                color: AppColor.secondaryColor,
                                borderRadius: BorderRadius.circular(13.5),
                              ),
                              child: const Icon(
                                Icons.add,
                                size: 14,
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              );
            }),
          ),
        ),
      ),
    );
  }
}

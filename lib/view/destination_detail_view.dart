import 'package:flutter/material.dart';
import 'package:travel_app/const.dart';
import 'package:travel_app/view/travel_plan_view.dart';

class DestinationDetailView extends StatelessWidget {
  const DestinationDetailView({super.key});

  @override
  Widget build(BuildContext context) {
    final count = ValueNotifier(
        0); // count buat durasi, pake value notifier supaya bisa diubah valuenya

    return Scaffold(
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            // panggil bottom sheet
            showModalBottomSheet(
              context: context,
              backgroundColor: Colors.white,
              builder: (context) {
                return Container(
                  margin: const EdgeInsets.all(30),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Mount Fuji',
                        style: AppTextStyle.titleTextStyle.copyWith(
                          fontWeight: FontWeight.w900,
                          fontSize: 24,
                        ),
                      ),
                      const SizedBox(height: 5),
                      Row(
                        children: [
                          const Icon(Icons.location_on_outlined, size: 14),
                          const SizedBox(width: 8),
                          Text(
                            'Honshu, Japan',
                            style: AppTextStyle.typograpyTextStyle.copyWith(
                              fontWeight: FontWeight.w700,
                              fontSize: 12,
                            ),
                          ),
                          const SizedBox(width: 8),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              // generate 5 icon bintang, pake ... (spread operator) buat nambahin iconnya ke row
                              ...List.generate(5, (index) {
                                return const Icon(
                                  Icons.star,
                                  color: Color(0xFFF4D150),
                                  size: 12,
                                );
                              }),
                              const SizedBox(width: 2),
                              Text(
                                '4.9',
                                style: AppTextStyle.typograpyTextStyle.copyWith(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 13,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      const SizedBox(height: 25),
                      Row(
                        children: [
                          Container(
                            width: 104,
                            color: const Color(0xFFF3F3F3),
                            child: Row(
                              children: [
                                // pake inkwell biar bisa dipencet
                                InkWell(
                                  borderRadius: BorderRadius.circular(13.5),
                                  onTap: () {
                                    // kalo dipencet, value countnya ngurang 1
                                    count.value--;
                                  },
                                  child: Container(
                                    height: 36,
                                    width: 29,
                                    decoration: BoxDecoration(
                                        color: AppColor.secondaryColor,
                                        borderRadius:
                                            BorderRadius.circular(13.5)),
                                    child: const Icon(
                                      Icons.remove,
                                      size: 14,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                Expanded(
                                  // pake value listenable builder supaya ke refresh kalau value count berubah
                                  child: ValueListenableBuilder(
                                    valueListenable: count,
                                    builder: (context, value, _) {
                                      return Text(
                                        value.toString(),
                                        textAlign: TextAlign.center,
                                        style: AppTextStyle.typograpyTextStyle
                                            .copyWith(
                                          fontWeight: FontWeight.w900,
                                          fontSize: 16,
                                        ),
                                      );
                                    },
                                  ),
                                ),
                                // pake inkwell biar bisa dipencet
                                InkWell(
                                  borderRadius: BorderRadius.circular(13.5),
                                  onTap: () {
                                    // kalo dipencet, value countnya nambah 1
                                    count.value++;
                                  },
                                  child: Container(
                                    height: 36,
                                    width: 29,
                                    decoration: BoxDecoration(
                                      color: AppColor.secondaryColor,
                                      borderRadius: BorderRadius.circular(13.5),
                                    ),
                                    child: const Icon(
                                      Icons.add,
                                      size: 14,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(width: 20),
                          Transform.flip(
                            child: const Icon(
                              Icons.access_time,
                              size: 24,
                              color: Colors.black,
                            ),
                          ),
                          const SizedBox(width: 7),
                          Text(
                            '5 Days',
                            style: AppTextStyle.typograpyTextStyle.copyWith(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 25),
                      Text(
                        'Description',
                        style: AppTextStyle.titleTextStyle.copyWith(
                          fontSize: 18,
                        ),
                      ),
                      const SizedBox(height: 18),
                      Text(
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Dignissim eget amet viverra eget fames rhoncus. Eget enim venenatis enim porta egestas malesuada et. Consequat mauris lacus euismod montes.',
                        style: AppTextStyle.titleTextStyle.copyWith(
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ),
                      ),
                      const SizedBox(height: 24),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Avg Accomodation Prices',
                                style: AppTextStyle.typograpyTextStyle.copyWith(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 12,
                                  color: AppColor.backgroundColor,
                                ),
                              ),
                              const SizedBox(height: 8),
                              Text(
                                ' \$200 - \$500',
                                style: AppTextStyle.typograpyTextStyle.copyWith(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 18,
                                  color: AppColor.backgroundColor,
                                ),
                              ),
                            ],
                          ),
                          ElevatedButton(
                            style: const ButtonStyle(
                              backgroundColor: MaterialStatePropertyAll(
                                AppColor.backgroundColor,
                              ),
                            ),
                            onPressed: () {
                              // pake navigator buat pindah ke travel plan kalau dipencet
                              Navigator.of(context).push(
                                MaterialPageRoute(builder: (context) {
                                  return const TravelPlanView();
                                }),
                              );
                            },
                            child: Text(
                              'Visit Now',
                              style: AppTextStyle.titleTextStyle.copyWith(
                                color: Colors.white,
                                fontSize: 14,
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                );
              },
            );
          },
          child: const Text('Click me'),
        ),
      ),
    );
  }
}

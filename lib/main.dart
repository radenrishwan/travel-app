import 'package:flutter/material.dart';
import 'package:travel_app/view/destination_detail_view.dart';

void main() {
  runApp(const InitialApp());
}

class InitialApp extends StatelessWidget {
  const InitialApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        useMaterial3: true,
      ),
      home: const DestinationDetailView(),
    );
  }
}
